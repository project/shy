<?php

namespace Drupal\shy\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;

/**
 * SHY Cleaner Filter class. Implements process() method only.
 *
 * @Filter(
 *   id = "shy_cleaner_filter",
 *   title = @Translation("Cleanup SHY markup"),
 *   description = @Translation("Remove <span> tag around <code>&amp;shy;</code>."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class ShyCleanerFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if ($filtered = $this->swapShyHtml($text)) {
      $result = new FilterProcessResult($filtered);
    }
    else {
      $result = new FilterProcessResult($text);
    }

    return $result;
  }

  /**
   * Replace <span class="shy"> tags with their respected HTML.
   *
   * @param string $text
   *   The HTML string to replace <span class="shy"> tags.
   *
   * @return string
   *   The HTML with all the <span class="shy">
   *   tags replaced with their respected html.
   */
  protected function swapShyHtml($text) {
    $document = Html::load($text);
    $xpath = new \DOMXPath($document);

    foreach ($xpath->query('//span[@class="shy"]') as $node) {
      if (!empty($node) && !empty($node->nodeValue)) {
        // PHP DOM removing the tag (not content)
        $node->parentNode->replaceChild(new \DOMText($node->nodeValue), $node);
      }
    }

    return Html::serialize($document);
  }

}
