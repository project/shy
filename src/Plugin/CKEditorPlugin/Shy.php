<?php

namespace Drupal\shy\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;

/**
 * Defines the "SHY" plugin.
 *
 * Plugin to insert a soft hyphen (&shy;) into the content
 * using the provided button.
 *
 * @CKEditorPlugin(
 *   id = "shy",
 *   label = @Translation("Soft hyphen"),
 *   module = "shy"
 * )
 */
class Shy extends CKEditorPluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('shy') . '/plugins/' . $this->getPluginId() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'DrupalShy' => [
        'label' => $this->t('Soft hyphen'),
        'image' => \Drupal::service('extension.list.module')->getPath('shy') . '/plugins/' . $this->getPluginId() . '/icons/' . $this->getPluginId() . '.png',
      ],
    ];
  }

}
