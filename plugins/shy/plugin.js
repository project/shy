/**
 * @file
 * insert soft hyphen for CKEditor
 */

/* global jQuery Drupal CKEDITOR */

(function($, Drupal, CKEDITOR) {
  "use strict";

  CKEDITOR.plugins.add("shy", {
    icons: "shy",
    hidpi: false,

    init: function (editor) {
      // Add &shy; widget.
      editor.widgets.add("insertShy", {
        template: '<span class="shy">&#173;</span>',
        draggable: false,
        allowedContent: ['span(!shy)'],
        // Position cursor after widget so users can keep on typing.
        init: function () {
          this.once('focus', function () {
            var range = editor.createRange();
            range.moveToPosition( this.wrapper, CKEDITOR.POSITION_AFTER_END );
            range.select();
          }, this);
        },
        upcast: function (element, data) {
          return element.name == 'span' && element.hasClass('shy');
        }
      });

      // Register the toolbar button.
      if (editor.ui.addButton) {
        editor.ui.addButton("DrupalShy", {
          label: Drupal.t("Soft hyphen"),
          command: "insertShy",
          icon: this.path + "icons/shy.png"
        });
      }
    }
  });
})(jQuery, Drupal, CKEDITOR);
